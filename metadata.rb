name 'phoenix'
maintainer 'Oleg Haidul'
maintainer_email 'oleghaidul@gmail.com'
description 'Installs and Configures Phoenix Framework'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version '0.1.0'
