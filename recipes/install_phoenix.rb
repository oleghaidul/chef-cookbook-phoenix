#
# Cookbook Name:: phoenix
# Recipe:: install_phoenix
#

phoenix_archive_url = "https://github.com/phoenixframework/archives/raw/master/phoenix_new-1.2.0.ez"

execute "Install Hex for Elixir" do
  command "mix local.hex --force"
  action  :run
end

execute "Install Phoenix Framework" do
  command "mix archive.install #{phoenix_archive_url} --force"
  action  :run
end
